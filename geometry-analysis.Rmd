---
title: "R Notebook"
output:
  html_document:
    df_print: paged
---

```{r}
library(tidyverse)
d = jsonlite::read_json("./data/geometry-data.json",
                          simplifyVector = TRUE) %>% 
  tibble() %>% 
  filter(key != "training") %>% 
  filter(trial_type == "experimental") %>% 
  filter(rt < 60000)
d
```

```{r}
group_info = jsonlite::read_json("./geometry_stimuli/stiminfo.json", simplifyVector = TRUE) %>% mutate(across(paper_category, as.factor))
group_info
```
```{r}
d = inner_join(d, group_info, by="group_number") %>%
  mutate(group_number = as.factor(group_number)) %>% 
  mutate(paper_category = as.factor(paper_category))
d
```


```{r}
d %>% ggplot(aes(paper_category, accuracy)) +
  stat_summary() +
  coord_flip()
```

```{r}
d %>% group_by(paper_category) %>% 
  summarize(p = {
    binom.test(sum(accuracy), length(accuracy), 0.5)$p.value
  }) 
```


```{r}
multcomp_results = d %>% 
  {glm(data = ., formula = accuracy ~ 1 + paper_category, family = "binomial")} %>% 
  multcomp::glht(linfct = multcomp::mcp(paper_category = "Tukey")) %>% 
  summary()
multcomp_results
```
```{r}
tibble(comparison = multcomp_results$test$tstat %>% names(),
       t = multcomp_results$test$tstat,
       p = multcomp_results$test$pvalues)
```